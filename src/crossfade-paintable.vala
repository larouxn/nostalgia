/* crossfade-paintable.vala
 *
 * Copyright 2021 Alexander Mikhaylenko
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nostalgia {
    public class CrossfadePaintable : Object, Gdk.Paintable {
        public Gdk.Paintable paintable { get; construct; }
        public Gdk.Paintable? prev_paintable { get; set construct; }

        private double _progress;
        public double progress {
            get { return _progress; }
            set {
                if (progress == value)
                    return;

                _progress = value;
                invalidate_contents ();
            }
        }

        public CrossfadePaintable (Gdk.Paintable paintable, Gdk.Paintable? prev_paintable) {
            Object (paintable: paintable, prev_paintable: prev_paintable);
        }

        public void snapshot (Gdk.Snapshot gdk_snapshot, double width, double height) {
            var snapshot = gdk_snapshot as Gtk.Snapshot;

            snapshot.push_cross_fade (progress);

            if (prev_paintable != null)
                prev_paintable.snapshot (gdk_snapshot, width, height);

            snapshot.pop ();
            paintable.snapshot (gdk_snapshot, width, height);
            snapshot.pop ();
        }
    }
}
