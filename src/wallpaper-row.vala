/* wallpaper-row.vala
 *
 * Copyright 2018 Tobias Bernard
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Nostalgia {
    [GtkTemplate (ui = "/org/gnome/Nostalgia/wallpaper-row.ui")]
    public class WallpaperRow : Gtk.ListBoxRow {
        private const int FADE_ANIM_MS = 200;

        private static TextureLoader loader;

        [GtkChild]
        private unowned Gtk.Picture wallpaper;

        public File file { get; construct; }
        public string version { get; construct; }
        public string date { get; construct; }
        public bool is_static { get; construct; }

        private Nostalgia.CrossfadePaintable? paintable;
        private int last_width;
        private int last_height;
        private bool loading;

        private Nostalgia.WallpaperPaintable? prev_wallpaper;

        public WallpaperRow.for_static (string filename, string version, string date, bool is_static) {
            var file = File.new_for_path (Config.WALLPAPERS_DIR + "/" + filename);

            Object (file: file, version: version, date: date, is_static: is_static);
        }

        public WallpaperRow.for_dynamic (string dir, string version, string date, bool is_static) {
            var file = File.new_for_path (Config.WALLPAPERS_DIR + "/" + dir + "/day.jpg");

            Object (file: file, version: version, date: date, is_static: is_static);
        }

        construct {
            notify["scale-factor"].connect (() => {
                maybe_load_texture (file);
            });

            maybe_load_texture (file);
        }

        static construct {
            loader = new TextureLoader ();
        }

        private void crossfade (Nostalgia.CrossfadePaintable crossfade) {
            var target = new Adw.CallbackAnimationTarget ((val) => {
                crossfade.progress = val;
            });
            var animation = new Adw.TimedAnimation (this, 0, 1, FADE_ANIM_MS, target);

            animation.done.connect (() => {
                crossfade.prev_paintable = null;
            });

            animation.play ();
        }

        public void maybe_load_texture (File file) {
            var width = wallpaper.width_request * scale_factor;
            var height = wallpaper.height_request * scale_factor;

            if (width == last_width && height == last_height && (loading || paintable != null))
                return;

            last_width = width;
            last_height = height;
            loading = true;

            loader.load (file, width, height, version, (w, h, _texture) => {
                loading = false;

                if (w == last_width && h == last_height) {
                    var new_wallpaper = new Nostalgia.WallpaperPaintable (_texture);
                    paintable = new Nostalgia.CrossfadePaintable (new_wallpaper,
                                                                  prev_wallpaper);

                    prev_wallpaper = new_wallpaper;
                    wallpaper.set_paintable (paintable);
                    crossfade (paintable);
                }
            });
        }
    }
}
